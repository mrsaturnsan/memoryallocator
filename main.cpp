#include "memoryallocator.h"

#include <iostream>

int main()
{
    ATL::TypeAllocator<int, 100> ta;
    int* array[100];

    for (int i = 0; i < 100; ++i)
        array[i] = ta.Allocate(i);
        
    for (int i = 99; i >= 0; --i)
    ta.Free(array[i]);
    
    return 0;
}
